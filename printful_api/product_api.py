from printful_api import PrintfulApiClient
from printful_api.model.sync.requests.sync_variant_request import SyncVariantRequest
from printful_api.model.sync.responses.sync_product_request_response import SyncProductRequestResponse
from printful_api.model.sync.responses.sync_product_response import SyncProductResponse
from printful_api.model.sync.responses.sync_products_response import SyncProductsResponse
from printful_api.model.sync.responses.sync_variant_response import SyncVariantResponse
from printful_api.model.sync.sync_product_creation_parameters import SyncProductCreationParameters

ENDPOINT_PRODUCTS = 'store/products'
ENDPOINT_VARIANTS = 'store/variants'


class ProductApi:
    """ API Docs: https://www.printful.com/docs/products """

    def __init__(self, client: PrintfulApiClient):
        self.client = client

    def get_products(self, offset: int = 0, limit: int = 20) -> SyncProductsResponse:
        f"""
        Performs GET SyncProducts request
        
        :param offset: 
        :param limit: 
        :return: {SyncProductsResponse}
        :raises PrintfulApiException, PrintfulException
        """
        request_params = {
            'offset': offset,
            'limit': limit
        }

        response = self.client.get(ENDPOINT_PRODUCTS, request_params)
        response_with_paging = {
            'result': response,
            'paging': self.client.last_response['paging']
        }
        return SyncProductsResponse.from_json(response_with_paging)

    def get_product(self, product_id: str) -> SyncProductRequestResponse:
        f"""
        Performs GET SyncProduct request
        
        :param product_id: 
        :return: {SyncProductRequestResponse}
        :raises PrintfulApiException, PrintfulException
        """
        response = self.client.get(f'{ENDPOINT_PRODUCTS}/{product_id}')
        return SyncProductRequestResponse.from_json(response)

    def get_variant(self, variant_id: str) -> SyncVariantResponse:
        f"""
        Performs GET SyncVariant request
        
        :param variant_id:
        :return: {SyncVariantResponse}
        :raises PrintfulApiException, PrintfulException
        """
        response = self.client.get(f'{ENDPOINT_VARIANTS}/{variant_id}')
        return SyncVariantResponse.from_json(response)

    def create_product(self, request: SyncProductCreationParameters) -> SyncProductResponse:
        f"""
        Performs POST SyncProduct request

        :param request: 
        :return: {SyncProductResponse} 
        :raises PrintfulApiException, PrintfulException, PrintfulSdkException
        """
        data = request.to_post_json()
        result = self.client.post(ENDPOINT_PRODUCTS, data)
        return SyncProductsResponse.from_json(result)

    def create_variant(self, product_id: str, sync_variant_request: SyncVariantRequest) -> SyncVariantResponse:
        f"""
        Performs POST SyncVariant request
        
        :param product_id: 
        :param sync_variant_request: 
        :return: {SyncVariantResponse} 
        :raises PrintfulApiException, PrintfulException, PrintfulSdkException
        """
        data = sync_variant_request.to_post_json()
        result = self.client.post(f'{ENDPOINT_PRODUCTS}/{product_id}/variants', data)
        return SyncVariantResponse.from_json(result)

    def update_product(self, product_id: str, request: SyncVariantRequest) -> SyncVariantResponse:
        f"""
        Performs PUT SyncProduct request
        
        :param product_id: 
        :param request: 
        :return: {SyncVariantResponse} 
        :raises PrintfulApiException, PrintfulException, PrintfulSdkException
        """
        data = request.to_put_json()
        result = self.client.put(f'{ENDPOINT_PRODUCTS}/{product_id}', data)
        return SyncProductResponse.from_json(result)

    def update_variant(self, variant_id: str, request: SyncVariantRequest) -> SyncVariantResponse:
        f"""
        Performs PUT SyncVariant request
        
        :param variant_id: 
        :param request: 
        :return: {SyncVariantResponse} 
        :raises PrintfulApiException, PrintfulException, PrintfulSdkException
        """
        data = request.to_put_json()
        result = self.client.put(f'{ENDPOINT_VARIANTS}/{variant_id}', data)
        return SyncVariantResponse.from_json(result)

    def delete_product(self, product_id: str) -> any:
        """
        Performs DELETE SyncProduct request
        :param product_id:
        :return: any
        :raises PrintfulApiException, PrintfulException
        """
        return self.client.delete(f'{ENDPOINT_PRODUCTS}/{product_id}')

    def delete_variant(self, variant_id: str) -> any:
        """
        Performs DELETE SyncVariant request

        :param variant_id:
        :return: any
        :raises PrintfulApiException, PrintfulException
        """
        return self.client.delete(f'{ENDPOINT_VARIANTS}/{variant_id}')
