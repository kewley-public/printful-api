from typing import NamedTuple, List

from printful_api.model.state_item import StateItem


class CountryItem(NamedTuple):
    code: str
    name: str
    states: List[any]

    @staticmethod
    def from_json(raw: dict):
        states = []
        if raw.get('states'):
            states = [StateItem.from_json(x) for x in raw.get('states', [])]
        return CountryItem(
            code=raw.get('code'),
            name=raw.get('name'),
            states=states
        )
