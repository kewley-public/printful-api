from typing import NamedTuple, List

from printful_api.exception.printful_sdk_exception import PrintfulSdkException
from printful_api.model.sync.requests.sync_product_request import SyncProductRequest
from printful_api.model.sync.requests.sync_variant_request import SyncVariantRequest


class SyncProductCreationParameters(NamedTuple):
    sync_product: SyncProductRequest
    sync_variants: List[SyncVariantRequest]

    @staticmethod
    def from_json(raw: dict):
        return SyncProductCreationParameters(
            sync_product=SyncProductRequest.from_json(raw.get('sync_product', {})),
            sync_variants=[SyncVariantRequest.from_json(x) for x in raw.get('sync_variants', [])]
        )

    def add_sync_variant(self, sync_variant: SyncVariantRequest):
        if self.sync_variants:
            self.sync_variants.append(sync_variant)
        else:
            self.sync_variants = [sync_variant]

    def to_post_json(self) -> dict:
        if self.sync_product:
            raise PrintfulSdkException('Missing product name')

        if self.sync_variants or len(self.sync_variants) < 1:
            raise PrintfulSdkException('No variants specified')

        post_json = {
            'sync_product': {
                'name': self.sync_product.name
            }
        }
        if self.sync_product.thumbnail:
            post_json['sync_product']['thumbnail'] = self.sync_product.thumbnail

        if self.sync_product.external_id:
            post_json['sync_product']['external_id'] = self.sync_product.external_id

        post_json['sync_variants'] = [x.to_post_json() for x in self.sync_variants]

        return post_json
