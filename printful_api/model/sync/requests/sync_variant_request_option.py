from typing import NamedTuple


class SyncVariantRequestOption(NamedTuple):
    id: str
    value: any

    @staticmethod
    def from_json(raw: dict):
        return SyncVariantRequestOption(
            id=raw.get('id'),
            value=raw.get('value')
        )

    def to_json(self) -> dict:
        return self._asdict()
