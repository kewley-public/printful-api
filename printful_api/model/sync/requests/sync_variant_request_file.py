from typing import NamedTuple

from printful_api.exception.printful_sdk_exception import PrintfulSdkException
from printful_api.model.file_item import FileItem, TYPE_DEFAULT


class SyncVariantRequestFile(NamedTuple):
    id: int
    url: str
    type: FileItem = TYPE_DEFAULT

    @staticmethod
    def from_json(raw: dict):
        return SyncVariantRequestFile(
            id=raw.get('id'),
            url=raw.get('url'),
            type=raw.get('type', TYPE_DEFAULT)
        )

    def to_json(self) -> dict:
        if self.id and self.url:
            raise PrintfulSdkException('Cannot specify both file id and url parameters')

        if self.id is None and self.url is None:
            raise PrintfulSdkException('Must specify file id or url parameter')

        json_dict = {}
        if self.id:
            json_dict['id'] = self.id

        if self.url:
            json_dict['url'] = self.url

        if self.type:
            json_dict['type'] = self.type

        return json_dict
