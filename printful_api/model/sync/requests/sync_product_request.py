from typing import NamedTuple


class SyncProductRequest(NamedTuple):
    external_id: str
    name: str
    thumbnail: str = ''

    @staticmethod
    def from_json(raw: dict):
        return SyncProductRequest(
            external_id=raw.get('external_id'),
            name=raw.get('name'),
            thumbnail=raw.get('thumbnail')
        )

    def to_json(self) -> dict:
        return self._asdict()
