from typing import NamedTuple, List

from printful_api.exception.printful_sdk_exception import PrintfulSdkException
from printful_api.model.sync.requests.sync_product_request import SyncProductRequest
from printful_api.model.sync.requests.sync_variant_request import SyncVariantRequest


class SyncProductUpdateParameters(NamedTuple):
    sync_product: SyncProductRequest
    sync_variants: List[SyncVariantRequest]

    @staticmethod
    def from_json(raw: dict):
        return SyncProductUpdateParameters(
            sync_product=SyncProductRequest.from_json(raw.get('sync_product', {})),
            sync_variants=[SyncVariantRequest.from_json(x) for x in raw.get('sync_variants', [])]
        )

    def to_put_json(self) -> dict:
        if self.sync_product is None and (self.sync_variants is None or len(self.sync_variants) < 1):
            raise PrintfulSdkException('Nothing to update')

        put_json = {}
        if self.sync_product:
            sync_product = {}
            if self.sync_product.name:
                sync_product['name'] = self.sync_product.name

            if self.sync_product.thumbnail:
                sync_product['thumbnail'] = self.sync_product.thumbnail

            if self.sync_product.external_id:
                sync_product['external_id'] = self.sync_product.external_id
            put_json['sync_product'] = sync_product

        put_json['sync_variants'] = [SyncVariantRequest.to_put_json(x) for x in self.sync_variants]
        return put_json
