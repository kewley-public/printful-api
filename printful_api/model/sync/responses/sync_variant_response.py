from typing import NamedTuple, List

from printful_api.model.file_item import FileItem
from printful_api.model.sync.responses.sync_variant_option_response import SyncVariantOptionResponse
from printful_api.model.sync.responses.sync_variant_product_response import SyncVariantProductResponse


class SyncVariantResponse(NamedTuple):
    id: int
    external_id: str
    sync_product_id: int
    name: str
    synced: bool
    variant_id: int
    retail_price: float
    currency: str
    product: SyncVariantProductResponse
    files: List[FileItem] = []
    options: List[SyncVariantOptionResponse] = []

    @staticmethod
    def from_json(raw: dict):
        return SyncVariantResponse(
            id=raw.get('id'),
            external_id=raw.get('external_id'),
            sync_product_id=raw.get('sync_product_id'),
            name=raw.get('name'),
            synced=raw.get('synced'),
            variant_id=raw.get('variant_id'),
            retail_price=raw.get('retail_price'),
            currency=raw.get('currency'),
            product=SyncVariantProductResponse.from_json(raw.get('product', {})),
            files=[FileItem.from_json(x) for x in raw.get('files', [])],
            options=[SyncVariantOptionResponse.from_json(x) for x in raw.get('options', [])]
        )
