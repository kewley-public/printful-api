from typing import NamedTuple


class SyncVariantProductResponse(NamedTuple):
    variant_id: int
    product_id: int
    image: str
    name: str

    @staticmethod
    def from_json(raw: dict):
        return SyncVariantProductResponse(
            variant_id=raw.get('variant_id'),
            product_id=raw.get('product_id'),
            image=raw.get('image', ''),
            name=raw.get('name', '')
        )
