from typing import NamedTuple


class SyncProductResponse(NamedTuple):
    id: int
    external_id: str
    name: str
    """ Total number of Sync Variants belonging to this product """
    variants: int = 0
    """ Number of synced Sync Variants belonging to this product """
    synced: int = 0

    @staticmethod
    def from_json(raw: dict):
        return SyncProductResponse(
            id=raw.get('id'),
            external_id=raw.get('external_id'),
            name=raw.get('name'),
            variants=raw.get('variants'),
            synced=raw.get('synced')
        )
