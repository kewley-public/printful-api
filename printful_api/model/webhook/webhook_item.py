from typing import NamedTuple, Optional

from printful_api.model.order.order import Order
from printful_api.model.shipment.shipment import Shipment


class WebhookItem(NamedTuple):
    type: str
    created: int
    retries: int
    store: int
    raw_data: any
    """ Reason why webhook was triggered. Present for some webhooks """
    reason: Optional[str]
    """ Present only for webhooks that contain order data """
    order: Optional[Order]
    shipment: Optional[Shipment]

    @staticmethod
    def from_json(raw: dict):
        order = None
        if raw.get('data', {}).get('order'):
            order = Order.from_json(raw['data']['order'])

        shipment = None
        if raw.get('data', {}).get('shipment'):
            shipment = Shipment.from_json(raw['data']['shipment'])

        reason = None
        if raw.get('data', {}).get('reason'):
            shipment = raw['data']['reason']

        return WebhookItem(
            type=raw.get('id'),
            created=raw.get('created'),
            retries=raw.get('retries'),
            store=raw.get('store'),
            raw_data=raw.get('data'),
            order=order,
            shipment=shipment,
            reason=reason
        )
