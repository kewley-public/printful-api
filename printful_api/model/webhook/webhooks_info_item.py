from typing import NamedTuple, List


class WebhooksInfoItem(NamedTuple):
    url: str
    types: List[any] = []
    params: List[any] = []

    @staticmethod
    def from_json(raw: dict):
        return WebhooksInfoItem(
            url=raw.get('url'),
            types=raw.get('types', []),
            params=raw.get('params', [])
        )
