""" Default placement (for posters, mugs, tc.) """
TYPE_DEFAULT = 'default'

""" Front placement for embroidery """
TYPE_EMBROIDERY_FRONT = 'embroidery_front'

""" Embroidery left side """
TYPE_EMBROIDERY_LEFT = 'embroidery_left'

""" Embroidery right side """
TYPE_EMBROIDERY_RIGHT = 'embroidery_right'

""" Back placement for embroidery """
TYPE_EMBROIDERY_BACK = 'embroidery_back'

""" Left chest placement for apparel embroidery """
TYPE_EMBROIDERY_CHEST_LEFT = 'embroidery_chest_left'

""" Front placement for apparel embroidery """
TYPE_EMBROIDERY_APPAREL_FRONT = 'embroidery_apparel_front'

""" Front for DTG products, double-sided totes, etc """
TYPE_FRONT = 'front'

""" Back for DTG products, double-sided totes, etc """
TYPE_BACK = 'back'

""" Inside label, for DTG products, except totes """
TYPE_LABEL_INSIDE = 'label_inside'

""" Outside label, for DTG products, except totes """
TYPE_LABEL_OUTSIDE = 'label_outside'

""" Belt front (leggings) """
TYPE_BELT_FRONT = 'belt_front'

""" Belt back (leggings) """
TYPE_BELT_BACK = 'belt_back'

""" Left sleeve (Cut & Sew, DTG shirts) """
TYPE_SLEEVE_LEFT = 'sleeve_left'

""" Right sleeve (Cut & Sew, DTG shirts) """
TYPE_SLEEVE_RIGHT = 'sleeve_right'

""" Mockup of the product, can be used when submitting orders, etc """
TYPE_MOCKUP = 'mockup'

""" Bikini top, backpack etc. """
TYPE_TOP = 'top'

TYPES = {
    f'{TYPE_DEFAULT}': {
        'title': 'Print file',
        'conflictingTypes': []
    },
    f'{TYPE_FRONT}': {
        'title': 'Front print',
        'conflictingTypes': []
    },
    f'{TYPE_BACK}': {
        'title': 'Back print',
        'conflictingTypes': [TYPE_LABEL_OUTSIDE]
    },
    f'{TYPE_EMBROIDERY_FRONT}': {
        'title': 'Front',
        'conflictingTypes': []
    },
    f'{TYPE_EMBROIDERY_LEFT}': {
        'title': 'Left side',
        'conflictingTypes': []
    },
    f'{TYPE_EMBROIDERY_RIGHT}': {
        'title': 'Right side',
        'conflictingTypes': []
    },
    f'{TYPE_EMBROIDERY_BACK}': {
        'title': 'Back',
        'conflictingTypes': []
    },
    f'{TYPE_EMBROIDERY_CHEST_LEFT}': {
        'title': 'Left chest',
        'conflictingTypes': []
    },
    f'{TYPE_EMBROIDERY_APPAREL_FRONT}': {
        'title': 'Front',
        'conflictingTypes': []
    },
    f'{TYPE_LABEL_INSIDE}': {
        'title': 'Inside label',
        'conflictingTypes': [TYPE_LABEL_OUTSIDE]
    },
    f'{TYPE_LABEL_OUTSIDE}': {
        'title': 'Outside label',
        'conflictingTypes': [TYPE_BACK, TYPE_LABEL_INSIDE]
    },
    f'{TYPE_SLEEVE_LEFT}': {
        'title': 'Left Sleeve',
        'conflictingTypes': []
    },
    f'{TYPE_SLEEVE_RIGHT}': {
        'title': 'Right Sleeve',
        'conflictingTypes': []
    },
    f'{TYPE_BELT_FRONT}': {
        'title': 'Front waist',
        'conflictingTypes': []
    },
    f'{TYPE_BELT_BACK}': {
        'title': 'Back waist',
        'conflictingTypes': []
    },
    f'{TYPE_TOP}': {
        'title': 'Top',
        'conflictingTypes': []
    },
}


def is_valid_placement(placement: str) -> bool:
    """
    Check whether a placement is valid
    :param placement:
    :return: bool T/F if it exists in the TYPES dict
    """
    return placement in TYPES
