from typing import NamedTuple, Optional


class AddressItem(NamedTuple):
    name: str
    company: str
    address1: str
    address2: Optional[str]
    city: str
    state_code: str
    state_name: str
    country_code: str
    country_name: str
    zip: str
    phone: str
    email: str

    @staticmethod
    def from_json(raw: dict):
        return AddressItem(
            name=raw.get('name'),
            company=raw.get('company'),
            address1=raw.get('address1'),
            address2=raw.get('address2'),
            city=raw.get('city'),
            state_code=raw.get('state_code'),
            state_name=raw.get('state_name'),
            country_code=raw.get('country_code'),
            country_name=raw.get('country_name'),
            zip=raw.get('zip'),
            phone=raw.get('phone'),
            email=raw.get('email')
        )

    def to_json(self) -> dict:
        return {
            'name': self.name,
            'company': self.company,
            'address1': self.address1,
            'address2': self.address2,
            'city': self.city,
            'stateCode': self.state_code,
            'countryCode': self.country_code,
            'countryName': self.country_name,
            'zip': self.zip,
            'phone': self.phone,
            'email': self.email
        }
