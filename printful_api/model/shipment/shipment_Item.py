from typing import NamedTuple


class ShipmentItem(NamedTuple):
    """ Line item ID """
    item_id: int
    quantity: int

    @staticmethod
    def from_json(raw: dict):
        return ShipmentItem(**raw)

    def to_json(self) -> dict:
        return {
            'itemId': self.item_id,
            'quantity': self.quantity
        }
