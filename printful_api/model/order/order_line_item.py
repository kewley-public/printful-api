from typing import NamedTuple, Optional, List

from printful_api.model.file_item import FileItem
from printful_api.model.order.order_item_option import OrderItemOption
from printful_api.model.order.product_variant import ProductVariant


class OrderLineItem(NamedTuple):
    id: int
    """ Line item ID from the external system """
    external_id: str
    variant_id: int
    quantity: int
    price: str
    """ Original retail price of the item to be displayed on the packing slip """
    retail_price: Optional[str]
    """ 
    Display name of the item. If not given, a name from the Printful system will
    be displayed on the packing slip 
    """
    name: str
    files: List[FileItem]
    product: ProductVariant
    options: List[OrderItemOption]
    """ Product identifier (SKU) from the external system """
    sku: str

    @staticmethod
    def from_json(raw: dict):
        return OrderLineItem(
            id=raw.get('id'),
            external_id=raw.get('external_id'),
            variant_id=raw.get('variant_id'),
            quantity=raw.get('quantity'),
            price=raw.get('price'),
            retail_price=raw.get('retail_price'),
            name=raw.get('name'),
            sku=raw.get('sku'),
            product=ProductVariant.from_json(raw.get('product', {})),
            files=[FileItem.from_json(x) for x in raw.get('files', [])],
            options=[OrderItemOption.from_json(x) for x in raw.get('options', [])]
        )

    def to_json(self) -> dict:
        files_json = []
        if len(self.files):
            files_json = [file.to_json() for file in self.files]

        options_json = []
        if len(self.options):
            options_json = [option.to_json() for option in self.options]

        retail_price = None
        if self.retail_price:
            retail_price = float(self.retail_price)

        return {
            'externalId': self.external_id,
            'variantId': self.variant_id,
            'quantity': self.quantity,
            'retailPrice': retail_price,
            'name': self.name,
            'sku': self.sku,
            'options': options_json,
            'files': files_json
        }
