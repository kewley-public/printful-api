from typing import NamedTuple


class PackingSlipItem(NamedTuple):
    email: str
    phone: str
    message: str

    @staticmethod
    def from_json(raw: dict):
        return PackingSlipItem(
            email=raw.get('email'),
            phone=raw.get('phone'),
            message=raw.get('message')
        )

    def to_json(self) -> dict:
        return {
            'email': self.email,
            'phone': self.phone,
            'message': self.message
        }
