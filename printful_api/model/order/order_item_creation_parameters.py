from typing import NamedTuple, Optional, List

from printful_api.model.file_item import FileItem
from printful_api.model.order.order_item_option import OrderItemOption


class OrderItemCreationParameters(NamedTuple):
    """ Line item ID from the external system """
    external_id: str
    """ Variant ID of the item ordered """
    variant_id: str
    quantity: int
    """ Original retail price of the item to be displayed on the packing slip """
    retail_price: Optional[str]
    """ 
    Display name of the item. If not given, a name from the Printful 
    system will be displayed on the packing slip 
    """
    name: Optional[str]
    """ Product identifier (SKU) from the external system """
    sku: str
    files: List[FileItem]
    options: List[OrderItemOption]

    def add_file(self, role: str, url: str, filename: str = None, id: str = None, visible: bool = True):
        """
        There are two ways to assign a print file to the item.
        One is to specify the File ID if the file already exists in the file library of the authorized store
        The second and the most convenient method is to specify the file URL.
        If a file with the same URL already exists, it will be reused
        :param role: Role of the file in the order
        :param url: Source URL where the file is downloaded from
        :param filename:
        :param id: ID if the file already exists in the Printfile Library
        :param visible: Show file in the Printfile Library (default true)
        """
        item = FileItem(
            type=role,
            url=url,
            filename=filename,
            id=id,
            visible=visible,
            created=None,
            dpi=None,
            status=None,
            mime_type=None,
            size=None,
            width=None,
            height=None,
            thumbnail_url=None,
            preview_url=None,
            hash=None
        )
        if len(self.files):
            self.files.append(item)
        else:
            self.files = [item]

    def add_option(self, id: str, value: str):
        """
        :param id:
        :param :value
        :return:
        """
        option = OrderItemOption(
            id=id,
            value=value
        )
        if len(self.options):
            self.options.append(option)
        else:
            self.options = [option]

    def to_json(self) -> dict:
        return {
            'external_id': self.external_id,
            'variant_id': self.variant_id,
            'quantity': self.quantity,
            'retail_price': self.retail_price,
            'name': self.name,
            'sku': self.sku,
            'files': self.files,
            'options': self.options
        }
