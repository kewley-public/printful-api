from typing import NamedTuple


class ProductVariant(NamedTuple):
    variant_id: int
    product_id: int
    image: str
    name: str

    @staticmethod
    def from_json(raw: dict):
        return ProductVariant(
            variant_id=raw.get('variant_id'),
            product_id=raw.get('product_id'),
            image=raw.get('image'),
            name=raw.get('name')
        )

    def to_json(self) -> dict:
        return {
            'variantId': self.variant_id,
            'productId': self.product_id,
            'image': self.image,
            'name': self.name
        }
