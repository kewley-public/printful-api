from typing import NamedTuple


class OrderItemOption(NamedTuple):
    id: str
    value: any

    @staticmethod
    def from_json(raw: dict):
        return OrderItemOption(
            id=raw.get('id'),
            value=raw.get('value')
        )

    def to_json(self) -> dict:
        return {
            'id': self.id,
            'value': self.value
        }
