from typing import NamedTuple, Optional

from printful_api.model.order.order_costs_item import OrderCostsItem


class OrderCostGroup(NamedTuple):
    printful_costs: Optional[OrderCostsItem]
    retail_costs: Optional[OrderCostsItem]

    @staticmethod
    def from_json(raw: dict):
        printful_costs = None
        if raw.get('costs'):
            printful_costs = OrderCostsItem.from_json(raw.get('costs'))

        retail_costs = None
        if raw.get('retail_costs'):
            retail_costs = OrderCostsItem.from_json(raw.get('retail_costs'))

        return OrderCostGroup(
            printful_costs=printful_costs,
            retail_costs=retail_costs
        )
