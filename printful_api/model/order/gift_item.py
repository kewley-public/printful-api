from typing import NamedTuple


class GiftItem(NamedTuple):
    subject: str
    message: str

    @staticmethod
    def from_json(raw: dict):
        return GiftItem(
            subject=raw.get('subject'),
            message=raw.get('message')
        )

    def to_json(self) -> dict:
        return {
            'subject': self.subject,
            'message': self.message
        }
