from typing import NamedTuple, List

from printful_api.model.order.order import Order


class OrderList(NamedTuple):
    total: int
    offset: int
    orders: List[Order]

    @staticmethod
    def from_raw(raw: List[dict], total: int, offset: int):
        return OrderList(
            total=total,
            offset=offset,
            orders=[Order.from_json(x) for x in raw]
        )
