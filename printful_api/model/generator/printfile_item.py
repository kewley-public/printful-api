from typing import NamedTuple

""" File should be fitted in the given area """
FILL_MODE_FIT = 'fit'
""" File should cover the whole area (sublimation, posters) """
FILL_MODE_COVER = 'cover'


class PrintfileItem(NamedTuple):
    printfile_id: int
    """ Width in pixels """
    width: int
    """ Height in pixels """
    height: int
    """ Default DPI for printfile """
    dpi: int
    """ Default mode for image. Should it cover the area or... """
    fill_mode: str
    """
    Indicates if printfile can be printed horizontal or vertical.
    This is useful for posters, canvas, where the orientation of the actual print can be changed to accommodate image orientation.
    That is, a 20x30 poster can be printed as 30x20 vertical poster if the iamge is vertical.
    """
    can_rotate: bool

    @staticmethod
    def from_json(raw: dict):
        return PrintfileItem(
            printfile_id=raw.get('printfile_id'),
            width=raw.get('width'),
            height=raw.get('height'),
            dpi=raw.get('dpi'),
            fill_mode=raw.get('fill_mode'),
            can_rotate=raw.get('can_rotate')
        )

    def get_ration(self) -> float:
        return self.width / self.height
