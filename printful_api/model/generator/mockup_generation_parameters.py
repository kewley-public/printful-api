from typing import NamedTuple, List, Optional

from printful_api.model.generator.mockup_generation_file import MockupGenerationFile
from printful_api.model.generator.mockup_position_item import MockupPositionItem

FORMAT_JPG = 'jpg'
FORMAT_PNG = 'png'


class MockupGenerationParameters(NamedTuple):
    """ Printful product id """
    product_id: int
    """ Products' Printful variant ids for which to generate mockups """
    variant_ids: List[int]
    files: List[MockupGenerationFile]
    """ 
    List of options to generate (Front, Back, etc)
    If not provided, everything is generated
    """
    options: List[str]
    """
    List of option groups to generate (for leggings Barefoot, High-heels, etc...)
    If not provided, everything is generated
    """
    option_groups: List[str]
    """
    Key-value list of product options.
    For example, ['stitch_color' => 'white'].
    Depends on the product, available option list can be found here:
    @link https://www.printful.com/docs/products#actionGet
    """
    product_options: dict
    """ Desired format. PNG supports transparent background. JPG is faster """
    format: str = FORMAT_JPG

    def add_image_url(
            self,
            placement: str,
            image_url: str,
            position: Optional[MockupPositionItem] = None
    ):
        file = MockupGenerationFile(
            placement=placement,
            image_url=image_url,
            position=position
        )
        if self.files:
            self.files.append(file)
        else:
            self.files = [file]
        return self

    def to_post_json(self) -> dict:
        files = []
        if self.files:
            files = [x.to_json() for x in self.files]

        return {
            'variant_ids': self.variant_ids,
            'files': files,
            'format': self.format,
            'option_groups': self.option_groups,
            'product_options': self.product_options,
            'options': self.options
        }
