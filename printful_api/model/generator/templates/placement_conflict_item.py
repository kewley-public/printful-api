from typing import NamedTuple, List


class PlacementConflictItem(NamedTuple):
    """ placement id """
    placement: str
    """ list of placement ids that are in conflict (cannot be used at the same time) """
    conflicting_placements: List[str]

    @staticmethod
    def from_json(raw: dict):
        return PlacementConflictItem(
            placement=raw.get('placement'),
            conflicting_placements=raw.get('conflicts', [])
        )
