from typing import NamedTuple, Optional

""" Horizontal template orientation (used for wall art) """
ORIENTATION_HORIZONTAL = 'horizontal'
""" Any template orientation (used for wall art, usually means that the template is a square) """
ORIENTATION_ANY = 'any'
""" Vertical template orientation (used for wall art) """
ORIENTATION_VERTICAL = 'vertical'


class TemplateItem(NamedTuple):
    """ Template's id """
    template_id: int
    """ 
    Main template image URL

    @see [is_template_on_front] - Indicates if this should be used as an overlay instead of a background 
    """
    image_url: str
    """ Indicates if the template image should be used as an overlay or a background for the positioning area"""
    is_template_on_front: bool
    """ Optional background template image URL """
    background_url: Optional[str]
    """ Optional HEX color code that should be set as template background color """
    background_color: Optional[str]
    """ 
    Id of the printfile that should be generated using given templates positions
    
    @see https://www.printful.com/docs/generator#actionPrintfiles
    """
    printfile_id: int
    """ Main area width that the template image should be used in """
    template_width: int
    """ Main area height that the template image should be used in """
    template_height: int
    """ Active positioning area width within the template area """
    print_area_width: int
    """ Active positioning area height within the template area """
    print_area_height: int
    """ Active positioning area top offset from the template area """
    print_area_top: int
    """ Active positioning area left offset from the template area """
    print_area_left: int
    orientation: str

    @staticmethod
    def from_json(raw: dict):
        return TemplateItem(
            template_item=raw.get('template_id'),
            image_url=raw.get('image_url'),
            is_template_on_front=raw.get('is_template_on_front'),
            background_url=raw.get('background_url'),
            background_color=raw.get('background_color'),
            printfile_id=raw.get('printfile_id'),
            template_width=raw.get('template_width'),
            template_height=raw.get('template_height'),
            print_area_width=raw.get('print_area_width'),
            print_area_height=raw.get('print_area_height'),
            print_area_top=raw.get('print_area_top'),
            print_area_left=raw.get('print_area_left'),
            orientation=raw.get('orientation')
        )
