from typing import NamedTuple, List

from printful_api.model.generator.templates.placement_conflict_item import PlacementConflictItem
from printful_api.model.generator.templates.template_item import TemplateItem
from printful_api.model.generator.templates.variant_template_mapping_item import VariantTemplateMappingItem


class ProductTemplates(NamedTuple):
    """ Indicates the version of resources. If this changes, resources should be re-cached """
    version: int
    """ Minimum DPI that is recommended for the user image """
    min_dpi: int
    """ Product variants mapped to templates """
    variant_mapping: List[VariantTemplateMappingItem]
    """ List of templates available. Use [variant_mapping] to link variants to templates """
    templates: List[TemplateItem]
    """ List of placement conflicts. This helps to determine which placement conflict (cannot be used together with)."""
    placement_conflicts: List[PlacementConflictItem]

    @staticmethod
    def from_json(raw: dict):
        variant_mapping = []
        if raw.get('variant_mapping'):
            variant_mapping = [VariantTemplateMappingItem.from_json(x) for x in raw.get('variant_mapping', [])]

        templates = []
        if raw.get('templates'):
            templates = [TemplateItem.from_json(x) for x in raw.get('templates', [])]

        placement_conflicts = []
        if raw.get('conflicting_placements'):
            placement_conflicts = [PlacementConflictItem.from_json(x) for x in raw.get('conflicting_placements', [])]

        return ProductTemplates(
            version=raw.get('version'),
            min_dpi=raw.get('min_dpi'),
            variant_mapping=variant_mapping,
            templates=templates,
            placement_conflicts=placement_conflicts
        )
