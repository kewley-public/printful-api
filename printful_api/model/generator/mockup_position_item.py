from typing import NamedTuple

from printful_api.exception.printful_exception import PrintfulException


class MockupPositionItem(NamedTuple):
    """ Positioning area width in pixels """
    area_width: float
    """ Positioning area height in pixels """
    area_height: float
    """ Image width within the area in pixels """
    width: float
    """ Image height within the area in pixels """
    height: float
    """ Image top offset from positioning area """
    top: float
    """ Image left offset from positioning area """
    left: float

    def to_json(self) -> dict:
        if self.width <= 0 or self.height <= 0:
            raise PrintfulException('Invalid size given for position item (less or equals zero)')

        return self._asdict()
