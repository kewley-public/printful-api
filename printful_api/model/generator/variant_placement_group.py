from typing import NamedTuple, List

from printful_api.model.generator.printfile_item import PrintfileItem


class VariantPlacementGroup(NamedTuple):
    """ Represents a group of variants that have the same placement and printfile """

    """ Placement identifier {Placements#TYPE_*} """
    placement: str
    printfile: PrintfileItem
    """ List of variant ids that are covered by this placement group """
    variant_ids: List[int]
