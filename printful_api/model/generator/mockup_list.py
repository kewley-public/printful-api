from typing import NamedTuple, List, Optional

from printful_api.model.generator.mockup_item import MockupItem


class MockupList(NamedTuple):
    mockups: List[MockupItem]

    @staticmethod
    def from_json(raw: dict):
        return MockupList(
            mockups=[MockupItem.from_json(x) for x in raw.get('mockups', [])]
        )

    def get_variant_mockups(self, variant_id: str, placement: Optional[str] = None) -> List[MockupItem]:
        """
        Filter mockups for a given variant id

        :param variant_id: variant identifier to filter on
        :param placement: Optional placement to filter mockups by
        :return: List[MockupItem]
        """
        return [
            item for item in self.mockups
            if variant_id in item.variant_ids and (placement is None or placement == item.placement)
        ]
