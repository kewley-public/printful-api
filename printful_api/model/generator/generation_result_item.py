from typing import NamedTuple, Optional

from printful_api.model.generator.mockup_list import MockupList

""" Generation task is in queue """
STATUS_PENDING = 'pending'
""" Generation has failed for some reason. Check error field. """
STATUS_FAILED = 'failed'
""" Generation is completed. """
STATUS_COMPLETED = 'completed'


class GenerationResultItem(NamedTuple):
    """ Unique task key, used to retrieve generation result """
    task_key: str
    """ Status of generation task. Potential values are above as constants"""
    status: str
    """ If task has failed, reason is given here """
    error: Optional[str]
    """ If generation is completed, mockup list is provided here """
    mockup_list: Optional[MockupList]

    def is_completed(self) -> bool:
        return self.status == STATUS_COMPLETED

    def is_pending(self) -> bool:
        return self.status == STATUS_PENDING

    def is_failed(self) -> bool:
        return self.status == STATUS_FAILED

    @staticmethod
    def from_json(raw: dict):
        mockup_list = None
        if raw.get('mockups'):
            mockup_list = MockupList.from_json(raw)

        return GenerationResultItem(
            task_key=raw.get('task_key'),
            status=raw.get('status'),
            error=raw.get('error'),
            mockup_list=mockup_list
        )
