from typing import NamedTuple


class VariantPrintfileItem(NamedTuple):
    variant_id: int
    """ Key is placement id, value is printfile id """
    placements: dict

    @staticmethod
    def from_json(raw: dict):
        return VariantPrintfileItem(
            variant_id=raw.get('variant_id'),
            placements=raw.get('placements')
        )
