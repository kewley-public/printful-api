from typing import NamedTuple


class MockupExtraItem(NamedTuple):
    """
    Title of the extra mockup, like "Wrinkled font", for mugs "Handle from left", etc.
    These values can change over time, do not hard-code / rely on them
    """
    title: str
    """ Url where mockup be downloaded from """
    url: str
    """ Mockup style """
    option: str
    """ Mockup style group """
    option_group: str

    @staticmethod
    def from_json(raw: dict):
        return MockupExtraItem(
            title=raw.get('title'),
            url=raw.get('url'),
            option=raw.get('option'),
            option_group=raw.get('option_group')
        )
