from typing import NamedTuple, Optional

from printful_api.model.generator.mockup_position_item import MockupPositionItem


class MockupGenerationFile(NamedTuple):
    """ Placement identifier (constant value from {Placements})"""
    placement: str
    image_url: str
    """ Optional positions for generation """
    position: Optional[MockupPositionItem]

    def to_json(self):
        json_dict = {
            'placement': self.placement,
            'image_url': self.image_url
        }
        if self.position:
            json_dict['position'] = self.position.to_json()

        return json_dict
