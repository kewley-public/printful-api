from typing import NamedTuple


class StateItem(NamedTuple):
    code: str
    name: str
    """ In some states shipping price is also included in tax calculation """
    shipping_taxable: bool

    @staticmethod
    def from_json(raw: dict):
        return StateItem(
            code=raw.get('code'),
            name=raw.get('name'),
            shipping_taxable=raw.get('shipping_taxable')
        )
