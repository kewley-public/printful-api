class PrintfulApiException(Exception):
    """Printful exception returned from the API."""
    pass
