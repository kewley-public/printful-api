import requests
from typing import Optional

from printful_api.exception.printful_api_exception import PrintfulApiException


class PrintfulApiClient:
    """
    Printful API class. Initializes the connection to the API server.
    :param key: API Key (Get it from your store's dashboard). Note that this
        is not the consumer key and secret found under store info. Rather,
        the API key can be found under Store > API and will be two strings
        separated by a ':'.
    :returns: A stateful object with an authenticated connection.
    """
    base_url: str = 'https://api.printful.com'
    auth_user: str
    auth_password: str
    session: requests.Session
    last_response: Optional[dict]
    last_response_raw: Optional[any]

    def __init__(self, api_key: str):
        if len(api_key) < 32:
            raise PrintfulApiException('Missing or invalid Printful store key!')

        self.auth_user, self.auth_password = api_key.split(':')
        print(f'{self.auth_user}:{self.auth_password}')
        self.setup_session()

    def setup_session(self):
        self.session = requests.Session()
        self.session.auth = (self.auth_user, self.auth_password)
        self.session.headers['User-Agent'] = 'kairaboudesign printful API'
        self.session.headers['Content-Type'] = 'application.json'

    def item_count(self) -> Optional[int]:
        """
        Returns total available item count from the last request if it supports paging (e.g. order list)
        or null otherwise
        """
        if self.last_response:
            return self.last_response.get('paging', {}).get('total')
        return None

    def get(self, path: str, params: dict = None) -> dict:
        """
        Perform a GET request to the API
        :param path: Request path (e.g. 'orders' or 'orders/123')
        :param params: Additional GET parameters as a dictionary
        :returns mixed API response
        """
        return self.request('GET', path, params)

    def delete(self, path: str, params: dict = None) -> dict:
        """
        Perform a DELETE request to the API
        :param path: Request path (e.g. 'orders' or 'orders/123')
        :param params: Additional GET parameters as a dictionary
        :returns mixed API response
        """
        return self.request('DELETE', path, params)

    def post(self, path: str, data: dict = None, params: dict = None) -> dict:
        """
        Perform a POST request to the API
        :param path: Request path (e.g. 'orders' or 'orders/123')
        :param data: Request body data as a dictionary
        :param params: Additional GET parameters as a dictionary
        :returns mixed API response
        """
        return self.request('POST', path, params, data)

    def put(self, path: str, data: dict = None, params: dict = None) -> dict:
        """
        Perform a PUT request to the API
        :param path: Request path (e.g. 'orders' or 'orders/123')
        :param data: Request body data as a dictionary
        :param params: Additional GET parameters as a dictionary
        :returns mixed API response
        """
        return self.request('PUT', path, params, data)

    def request(self, method: str, path: str, params: dict = None, data: dict = None):
        self.last_response = None
        self.last_response_raw = None
        url = f'{self.base_url}/{path}'

        try:
            response = self.session.request(
                method,
                url,
                params=params,
                data=data
            )
            self.last_response_raw = response
        except Exception as e:
            raise PrintfulApiException(f'API request failed {e}')

        if response.status_code < 200 or response.status_code >= 300:
            raise PrintfulApiException(f'Invalid API response {response.status_code}')

        try:
            data = response.json()
            self.last_response = data
        except ValueError as e:
            raise PrintfulApiException('API Response was not valid JSON.')

        return data['result']
