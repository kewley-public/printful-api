from typing import Optional, List, Dict
import time

from printful_api import PrintfulApiClient
from printful_api.model.generator.generation_result_item import GenerationResultItem
from printful_api.model.generator.mockup_generation_parameters import MockupGenerationParameters
from printful_api.model.generator.product_printfiles import ProductPrintfiles
from printful_api.model.generator.templates.product_templates import ProductTemplates
from printful_api.model.generator.variant_placement_group import VariantPlacementGroup


class MockupGeneratorApi:
    """ API Docs: https://www.printful.com/docs/generator """

    def __init__(self, client: PrintfulApiClient):
        self.client = client

    def get_product_print_files(
            self,
            product_id: int,
            orientation: Optional[str] = None
    ) -> ProductPrintfiles:
        f"""
        Get all available templates for specific printful product
        
        :param product_id: Printful product id 
        :param orientation: Used for products with multiple orientations (e.g. wall art) (see TemplateItem for variations)
        :return: {ProductPrintfiles}
        """

        query = {}
        if orientation:
            query['orientation'] = orientation

        response = self.client.get(f'mockup-generator/printfiles/{product_id}', query)
        return ProductPrintfiles.from_json(response)

    def group_printfiles(self, product_print_files: ProductPrintfiles) -> List[VariantPlacementGroup]:
        f"""
        Merge variants into unique printfile + placement groups.
        This group can be used for positioning, that covers a list of variants
        
        :param product_print_files: 
        :return: {List[VariantPlacementGroup]} 
        """
        response: Dict[str, VariantPlacementGroup] = {}

        for variant in product_print_files.variant_printfiles:
            for [key, value] in variant.placements:
                key = f'{key}:{value}'
                variant_ids = [variant.variant_id]
                if key in response:
                    variant_ids = variant_ids + response[key].variant_ids
                response[key] = VariantPlacementGroup(
                    placement=key,
                    printfile=product_print_files.printfiles[value],
                    variant_ids=variant_ids
                )
        return list(response.values())

    def create_generation_task(self, parameters: MockupGenerationParameters) -> GenerationResultItem:
        f"""
        Create an asynchronous generation task and return task that is in pending state.
        To retrieve the generation result use {MockupGeneratorApi.get_generation_task}
        :param parameters:
        :return: {GenerationResultItem}
        :raises: PrinfulException
        """
        data = parameters.to_post_json()
        response = self.client.post(f'/mockup-generator/create-task/{parameters.product_id}', data)
        return GenerationResultItem.from_json(response)

    def create_generation_task_and_wait_for_result(
            self,
            parameters: MockupGenerationParameters,
            max_seconds_wait: int = 180,
            interval: int = 5
    ) -> GenerationResultItem:
        f"""
        Create a task and waits for it to be complete by periodically checking for result.
        If the timeout is exceeded, latest task result is returned which will be in pending state.
        
        :param parameters: 
        :param max_seconds_wait: Maximum amount of seconds to wait for the result 
        :param interval: Interval before request task result
        :return: {GenerationResultItem} completed or failed generation result
        :raises: PrintfulException
        """
        task = self.create_generation_task(parameters)
        for x in range(0, int(max_seconds_wait / interval)):
            time.sleep(interval)
            task = self.get_generation_task(task.task_key)
            if not task.is_pending():
                break
        return task

    def get_generation_task(self, task_key: str) -> GenerationResultItem:
        """
        Check for a generation task result

        :param task_key:
        :return:
        :raises: PrintfulException
        """
        query = {
            'task_key': task_key
        }
        response = self.client.get('mockup-generator/task', query)
        return GenerationResultItem.from_json(response)

    def get_product_templates(self, product_id: int, orientation: Optional[str]) -> ProductTemplates:
        f"""
        Retrieve templates for given product. Rsources returned may be used to create your own generator interface.
        This includes background images and area positions.
        :param product_id: 
        :param orientation: Used for products with multiple orientations (e.g. wall art) (see TemplateItem for variations) 
        :return: {ProductTemplates}
        :raises: PrintfulApiException, PrintfulException
        """
        query = {}
        if orientation:
            query['orientation'] = orientation

        response = self.client.get(f'mockup-generator/templates/{product_id}', query)
        return ProductTemplates.from_json(response)
