from typing import Optional

from printful_api.model.order import order_creation_parameters
from printful_api.model.order.order import Order
from printful_api.model.order.order_cost_group import OrderCostGroup
from printful_api.model.order.order_list import OrderList
from printful_api.printful_api_client import PrintfulApiClient


class OrderApi:
    """ API Docs: https://www.printful.com/docs/orders """

    def __init__(self, client: PrintfulApiClient):
        self.client = client

    def create(self, parameters: order_creation_parameters, confirm: bool = False) -> Order:
        f"""
        Performs a POST Order request

        :param parameters:
        :param confirm: Automatically submit the newly created order for fulfillment (skip the Draft phase)
        :return: {Order}
        """
        request = parameters.to_request_parameters()
        raw = self.client.post('orders', request, {'confirm': confirm})
        return Order.from_json(raw)

    def update(self, parameters: order_creation_parameters, order_id: int, confirm: bool = False) -> Order:
        f"""
        Updates unsubmitted order and optionally submits it for the fulfillment.
        Post only the fields that need to be changed, not all required fields.

        :param parameters:
        :param order_id: Order ID (integer)
        :param confirm: Automatically submit the newly created order for fulfillment (skip the Draft phase
        :return: {Order}
        """
        request = parameters.to_request_parameters()
        raw = self.client.put(f'orders/{order_id}', request, {'confirm': confirm})
        return Order.from_json(raw)

    def update_external(self, parameters: order_creation_parameters, external_id: str, confirm: bool = False) -> Order:
        f"""
        Updates unsubmitted order and optionally submits it for the fulfillment.
        Post only the fields that need to be changed, not all required fields.

        :param parameters:
        :param external_id:  External ID (prefixed with @)
        :param confirm: Automatically submit the newly created order for fulfillment (skip the Draft phase
        :return: {Order}
        """
        request = parameters.to_request_parameters()
        raw = self.client.put(f'orders/{external_id}', request, {'confirm': confirm})
        return Order.from_json(raw)

    def cancel(self, order_id: int) -> Order:
        f"""
        Sets the order status to CANCELED

        :param order_id: Order ID (integer)
        :return: {Order} 
        """
        raw = self.client.delete(f'orders/{order_id}')
        return Order.from_json(raw)

    def cancel_external(self, external_id: str) -> Order:
        f"""
        Sets the order status to CANCELED

        :param external_id: External ID (prefixed with @)
        :return: {Order} 
        """
        raw = self.client.delete(f'orders/{external_id}')
        return Order.from_json(raw)

    def confirm(self, order_id: int) -> Order:
        f"""
        Performs a POST Order Confirmation request by id
        
        :param order_id: Order ID (integer)
        :return: {Order}
        """
        raw = self.client.post(f'orders/{order_id}/confirm')
        return Order.from_json(raw)

    def confirm_external(self, external_id: str) -> Order:
        f"""
        Performs a POST Order Confirmation request by external_id
        
        :param external_id: External ID (prefixed with @)
        :return: {Order}
        """
        raw = self.client.post(f'orders/{external_id}/confirm')
        return Order.from_json(raw)

    def get_by_id(self, order_id: int) -> Order:
        f"""
        Performs a GET Order request by id
        
        :param order_id: Order ID (integer)
        :return: {Order}
        """
        raw = self.client.get(f'orders/{order_id}')
        return Order.from_json(raw)

    def get_by_external_id(self, external_id: str) -> Order:
        f"""
        Performs a GET Order request by external_id (e.g. @1234)
        
        :param external_id: External ID (prefixed with @)
        :return: {Order}
        """
        sanitized = f'@{external_id.lstrip("@")}'
        raw = self.client.get(f'orders/{sanitized}')
        return Order.from_json(raw)

    def get_by_printful_id(self, printful_id: int) -> Order:
        f"""
        Performs a GET Order request by printful identifier
        
        :param printful_id: 
        :return: {Order} 
        """
        return self.get_by_id(printful_id)

    def get_list(self, offset: int = 0, limit: int = 10, status: Optional[str] = None):
        f"""
        Performs a GET Order request with paging and an optional status filter

        :param offset:
        :param limit: Number of items per page (max 100)
        :param status: Filter by order status
        :return: {OrderList}
        """
        params = {
            'offset': offset,
            'limit': limit,
        }
        if status:
            params['status'] = status

        raw_orders = self.client.get('orders', params)
        total = self.client.item_count()
        return OrderList.from_raw(raw=raw_orders, total=total, offset=offset)

    def estimate_costs(self, parameters: order_creation_parameters) -> OrderCostGroup:
        f"""

        :param parameters: 
        :return: {OrderCostGroup}
        :raises PrintfulApiException, PrintfulApiException
        """
        request = parameters.to_request_parameters()
        raw = self.client.post('orders/estimate-costs', request)
        return OrderCostGroup.from_json(raw)
