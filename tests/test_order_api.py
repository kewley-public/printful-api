from unittest import TestCase
import requests_mock
import json

from httmock import urlmatch, HTTMock, response

from printful_api import OrderApi
from printful_api.printful_api_client import PrintfulApiClient

TEST_ENDPOINT = 'mock://test-api.com'


class TestOrderApi(TestCase):
    def setUp(self) -> None:
        client = PrintfulApiClient('')
        self.adapter = requests_mock.Adapter()
        client.session.mount('mock://', self.adapter)

        client.base_url = TEST_ENDPOINT
        self.api = OrderApi(client)

    def test_sanity(self):
        self.assertEqual(True, True)

    # def get_items_response(self, request):
    #     self.assertEqual('GET', request.method)
    #     request_body = json.loads(request.body)
    #     print(request_body)
    #     return response(
    #         status_code=200,
    #         content=[]
    #     )
    #
    # def test_get_items(self):
    #     self.adapter.register_uri(
    #         'GET',
    #         f'{TEST_ENDPOINT}/orders?offset=0&limit=10&status=',
    #         additional_matcher=self.get_items_response
    #     )
    #     list = self.api.get_list(0, 10)
    #
    #     print(list)
    #     self.assertEqual(True, True)
    #
    # def test_get_id(self):
    #     order = self.api.get_by_id(38345030)
    #     print(order)
    #     self.assertEqual(True, True)
    #
    # def test_get_by_external_id(self):
    #     order = self.api.get_by_external_id('@9887112')
    #     print(order)
    #     self.assertEqual(True, True)
